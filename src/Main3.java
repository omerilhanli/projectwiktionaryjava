import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/*
 * H2 den sonra ikinci H2ye kadar, H3-P-OL 3lüsünü alıp basalım
 */
public class Main3 {

	public static void main(String[] args) {

		findMean("on", "en");
	}

	static String findMean(String text, String lngCd) {

		Element element = findOL(text, lngCd);

		long start = System.currentTimeMillis();

		List<Mean> meanList = findMeanList(element);

		getTime(start);

		for (int j = 0; j < meanList.size(); j++) {

			Mean mean = meanList.get(j);

			System.err.println("size:" + mean.getTextExampleList().size() + " - text:" + mean.getTextDefinition());
		}

		return "";
	}

	static List<Mean> findMeanList(Element element) {

		Elements elements = element.select("ol li");

		List<Mean> meanList = new ArrayList<>();

		List<String> quotations = new ArrayList<>();

		// MeanList create
		for (int i = 0; i < elements.size(); i++) {

			Element elementItem = elements.get(i);

			Mean mean = new Mean();

			String txtDefinition = elementItem.text().trim();

			if (!txtDefinition.isEmpty()) {

				mean.setTextDefinition(txtDefinition);
			}

			Elements uls = elementItem.select("ul li");

			for (int j = 0; j < uls.size(); j++) {

				Element ul = uls.get(j);

				String ulItem = ul.text();

				if (!quotations.contains(ulItem)) {

					quotations.add(ulItem);
				}
			}

			Elements dls = elementItem.select("dl");

			List<String> exampleList = new ArrayList<>();

			for (int j = 0; j < dls.size(); j++) {

				Element dd = dls.get(j);

				Elements elsI = dd.select("dd i");

				for (int k = 0; k < elsI.size(); k++) {

					String ddTitle = elsI.get(k).text().trim();

					if (!ddTitle.isEmpty()) {

						exampleList.add(ddTitle);
					}
				}
			}

			mean.setTextExampleList(exampleList);
			meanList.add(mean);
		}
		// ---------------------------------------------------------------

		// Quotations remove from MeanList
		for (int i = 0; i < meanList.size(); i++) {

			Mean mean = meanList.get(i);

			String definition = mean.getTextDefinition();

			for (int j = 0; j < quotations.size(); j++) {

				String quot = quotations.get(j);

				if (definition.contains(quot)) {

					definition = definition.replace(quot, "");
				}
			}

			mean.setTextDefinition(definition.isEmpty() ? "" : definition);
		}

		System.err.println("Before meanList.size:" + meanList.size());
		List<Integer> removeIndexList = new ArrayList<>();
		for (int i = 0; i < meanList.size(); i++) {
			if (meanList.get(i).getTextDefinition().isEmpty()) {
				removeIndexList.add(i);
			}
		}

		int removeCount = 0;

		for (int i = 0; i < removeIndexList.size(); i++) {

			int ndx = removeIndexList.get(i);

			meanList.remove(ndx - removeCount);

			removeCount++;
		}
		System.err.println("A meanList.size:" + meanList.size());
		// ---------------------------------------------------------------
		return meanList;
	}

	static Element findOL(String txt, String lngCd) {

		try {

			String tagH2 = "h2:nth";

			String tagOL = "ol:nth";

			String url = "https://" + lngCd.trim() + ".wiktionary.org/wiki";

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", txt).post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			int countH2 = 0;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(tagH2)) {

					countH2++;
				}

				if (countH2 == 1) {

					if (cssSelector.contains(tagOL)) {

						return elementChild;
					}
				}
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static void getTime(long startTime) {

		// Toplam süre hesaplanır
		long total = System.currentTimeMillis() - startTime;

		long min = TimeUnit.MILLISECONDS.toMinutes(total);

		long sec = TimeUnit.MILLISECONDS.toSeconds(total)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(total));

		long millis = total - TimeUnit.MILLISECONDS.toSeconds(total) * 1000;

		String time = String.format("%d min %d sec", min, sec);

		System.err.println("time: " + time + " - millis:" + millis);
	}

}
