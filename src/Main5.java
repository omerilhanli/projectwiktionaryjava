import java.util.concurrent.TimeUnit;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Main5 {

	public static void main(String[] args) {

		String tagH2 = "h2:nth";

		String tagOL = "ol:nth";

		String url = "https://en.wiktionary.org/wiki";

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			long start = System.currentTimeMillis();

			Document document = con.data("search", "on").maxBodySize(0).post();

			getTime(start);

//			System.err.println("document:" + document.toString());"

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static void getTime(long startTime) {

		// Toplam süre hesaplanır
		long total = System.currentTimeMillis() - startTime;

		long min = TimeUnit.MILLISECONDS.toMinutes(total);

		long sec = TimeUnit.MILLISECONDS.toSeconds(total)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(total));

		long millis = total - TimeUnit.MILLISECONDS.toSeconds(total) * 1000;

		String time = String.format("%d min %d sec", min, sec);

		System.err.println("time: " + time + " - millis:" + millis);
	}
}
