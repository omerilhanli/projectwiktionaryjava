import java.util.List;

public class Mean {

	private String textDefinition;

	private List<String> textExampleList;

	public Mean() {

	}

	public String getTextDefinition() {
		return textDefinition;
	}

	public void setTextDefinition(String textDefinition) {
		this.textDefinition = textDefinition;
	}

	public List<String> getTextExampleList() {
		return textExampleList;
	}

	public void setTextExampleList(List<String> textExampleList) {
		this.textExampleList = textExampleList;
	}

}
