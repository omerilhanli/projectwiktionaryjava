
import org.jsoup.nodes.Element;

public class WktMeanPair {

	private int index;

	private Element element;

	private String tag;

	private String text;

	public WktMeanPair() {

	}

	public WktMeanPair(int index, String tag, String text) {
		super();
		this.index = index;
		this.tag = tag;
		this.text = text;
	}

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
