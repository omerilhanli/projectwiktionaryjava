import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

public class Main {

	static String url = "https://en.wiktionary.org/wiki";

	static String tagH2 = "h2:nth";
	static String tagH3 = "h3:nth";
	static String patternH3 = "h3 span.mw-headline";

	static String tagH4 = "h4:nth";
	static String patternH4 = "h4 span.mw-headline";

	static String tagH5 = "h5:nth";
	static String patternH5 = "h5 span.mw-headline";

	static String tagP = "p:nth";
	// Sırasız
	static String tagUL = "ul:nth";
	// Sıralı
	static String tagOL = "ol:nth";

	public static void main(String[] args) {

		// List<WktMeanPair> listFirstH2 = findFirstTagList(tagH2);
		//
		// for (WktMeanPair pair : listFirstH2) {
		// System.err.println("index:" + pair.getIndex() + " - tag:" + pair.getTag() +
		// "- text:" + pair.getText());
		//
		// }

		// Kelime Anlamı
		long startTime = System.currentTimeMillis();

		List<WktMeanPair> listH4ToH5 = findTagToTag(tagOL, tagH5);

		for (WktMeanPair pair : listH4ToH5) {
			// System.err.println("index:" + pair.getIndex() + " - tag:" + pair.getTag() +
			// "- text:" + pair.getText());
			Element element = pair.getElement();
			printElement(element); // Tüm kelimeleri yazdırma
		}

		// Toplam süre hesaplanır
		long total = System.currentTimeMillis() - startTime;
		System.err.println("total:" + total);

		long min = TimeUnit.MILLISECONDS.toMinutes(total);

		long sec = TimeUnit.MILLISECONDS.toSeconds(total)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(total));

		long millis = total - TimeUnit.MILLISECONDS.toSeconds(total) * 1000;

		String time = String.format("%d.%d,%d min", min, sec, millis);

		System.err.println("time: " + time);
	}

	static void printElement(Element element) {

		Elements elements = element.select("li");

		List<String> ulList = new ArrayList<>();
		List<String> allList = new ArrayList<>();

		for (int i = 0; i < elements.size(); i++) {

			Element elementAll = elements.get(i);

			allList.add(elementAll.text().trim());

			Elements dls = elementAll.select("dl");

			// OL liste elemanları
			for (int j = 0; j < dls.size(); j++) {
				Element dd = dls.get(j);
				Elements els = dd.select("dd div.h-usage-example");
				for (int k = 0; k < els.size(); k++) {
					String ddTitle = els.get(k).text();
					if (!ddTitle.isEmpty()) {
						// System.err.println(i + ". ddTitle:" + ddTitle);
					}
				}
			}

			Elements uls = elementAll.select("ul li");
			for (int j = 0; j < uls.size(); j++) {
				ulList.add(uls.get(j).text());
			}
		}

		for (int j = 0; j < allList.size(); j++) {
			String allStr = allList.get(j);
			for (int k = 0; k < ulList.size(); k++) {
				String ulStr = ulList.get(k);
				if (allStr.contains(ulStr)) {
					allStr = allStr.replace(ulStr, "");
				}
				allList.set(j, allStr);
			}
		}

		for (int j = 0; j < allList.size(); j++) {
			if (!allList.get(j).isEmpty())
				System.err.println(j + ". allList.get:" + allList.get(j));
		}

	}

	static List<WktMeanPair> findTagToTag(String fromTag, String toTag) {

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", "on").post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			List<WktMeanPair> list = new ArrayList<>();

			int count = 0;

			int index = 0;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(fromTag) || cssSelector.contains(toTag)) {

					count++;
				}

				if (count == 1) {

					if (checkTagContain(cssSelector)) {

						// System.err.println("cssSelector:" + cssSelector);

						WktMeanPair mainPair = new WktMeanPair(index++, elementTag(elementChild.cssSelector()),
								elementChild.text().trim());

						list.add(mainPair);

						mainPair.setElement(elementChild);
					}
				}
			}

			return list;

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static List<WktMeanPair> findFirstTagList(String fromTag) {

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", "on").post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			List<WktMeanPair> list = new ArrayList<>();

			int count = 0;

			int index = 0;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(fromTag)) {

					count++;
				}

				if (count == 1) {

					if (checkTagContain(cssSelector)) {

						// System.err.println("cssSelector:" + cssSelector);

						WktMeanPair mainPair = new WktMeanPair(index++, elementTag(elementChild.cssSelector()),
								elementChild.text().trim());

						mainPair.setElement(elementChild);

						list.add(mainPair);
					}
				}
			}

			return list;

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static String elementTag(String cssSelector) {

		if (cssSelector.contains(tagOL)) {
			return tagOL;
		} else if (cssSelector.contains(tagH3)) {
			return tagH3;
		} else if (cssSelector.contains(tagH4)) {
			return tagH4;
		} else if (cssSelector.contains(tagH5)) {
			return tagH5;
		} else if (cssSelector.contains(tagP)) {
			return tagP;
		} else if (cssSelector.contains(tagUL)) {
			return tagUL;
		} else {
			return tagH2;
		}
	}

	static boolean checkTagContain(String cssTag) {

		if (cssTag.contains(tagH2) || cssTag.contains(tagH3) || cssTag.contains(tagH4) || cssTag.contains(tagH5)
				|| cssTag.contains(tagP) || cssTag.contains(tagUL) || cssTag.contains(tagOL)) {

			return true;
		}

		return false;
	}

	// public static int getSortElement(String cssTag) {
	//
	// if (!checkTagContain(cssTag)) {
	//
	// return -1;
	// }
	//
	// String patternChildNumber = "\\((.+?)\\)";
	//
	// Pattern pattern = Pattern.compile(patternChildNumber);
	//
	// Matcher matcher = pattern.matcher(cssTag);
	//
	// int number = -1;
	//
	// if (matcher.find()) {
	//
	// String n = matcher.group(1);
	//
	// if (!n.isEmpty()) {
	//
	// number = Integer.valueOf(n);
	// }
	// }
	//
	// return number;
	// }

	// static void printUL(Element element) {
	//
	// Elements elementsUL = element.select("ul");
	//
	// for (int i = 0; i < elementsUL.size(); i++) {
	//
	// Element elementUL = elementsUL.get(i);
	//
	// Elements elementsLI = elementUL.children();
	//
	// for (int j = 0; j < elementsLI.size(); j++) {
	//
	// Element elementLI = elementsLI.get(j);
	//
	// System.err.println(j + ". LU j-elementLI.text() : " + elementLI.text());
	// }
	// System.err.println();
	// }
	// }
}
