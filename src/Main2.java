import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

/*
 * H2 den sonra ikinci H2ye kadar, H3-P-OL 3lüsünü alıp basalım
 */
public class Main2 {

	static String url = "https://en.wiktionary.org/wiki";

	static String tagH2 = "h2:nth";

	static String patternH3 = "h3 span.mw-headline";

	static String tagH4 = "h4:nth";
	static String tagP = "p:nth";
	static String tagOL = "ol:nth";

	public static void main(String[] args) {

		 printOL();
	}

	static WktMeanPair findH4ToPToOL() {

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", "on").post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			int countH2 = 0;

			int index = 0;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(tagH2)) {

					countH2++;
				}

				if (countH2 == 1) {

					if (cssSelector.contains(tagOL)) {

						WktMeanPair pair = new WktMeanPair();

						pair.setElement(elementChild);

						pair.setIndex(index);

						pair.setText(elementChild.text().trim());

						pair.setTag(tagOL);

						return pair;
					}
				}
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static Element findTag(String tag) {

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", "on").post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			Element elementTarget = null;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(tag)) {

					System.err.println("elementChild:" + elementChild.text());

					elementTarget = elementChild;

					printElement(elementChild);

					break;
				}
			}

			return elementTarget;

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static void printOL() {

		List<WktMeanPair> listFirstH2 = findFirstTagList(tagOL);

		for (WktMeanPair pair : listFirstH2) {
			System.err.println("index:" + pair.getIndex() + " - tag:" + pair.getTag() + "- text:" + pair.getText());
		}
	}

	static void printOLToH3() {

		// Kelime Anlamı
		long startTime = System.currentTimeMillis();

		List<WktMeanPair> listH4ToH5 = findTagToTag2(tagOL, tagH4);

		for (WktMeanPair pair : listH4ToH5) {
			// System.err.println("index:" + pair.getIndex() + " - tag:" + pair.getTag() +
			// "- text:" + pair.getText());
			Element element = pair.getElement();
			printElement(element); // Tüm kelimeleri yazdırma
		}

		// Toplam süre hesaplanır
		long total = System.currentTimeMillis() - startTime;

		System.err.println("total:" + total);

		long min = TimeUnit.MILLISECONDS.toMinutes(total);

		long sec = TimeUnit.MILLISECONDS.toSeconds(total)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(total));

		long millis = total - TimeUnit.MILLISECONDS.toSeconds(total) * 1000;

		String time = String.format("%d.%d,%d min", min, sec, millis);

		System.err.println("time: " + time);
	}

	static void printElement(Element element) {

		Elements elements = element.select("li");

		List<String> ulList = new ArrayList<>();
		
		List<String> allList = new ArrayList<>();

		for (int i = 0; i < elements.size(); i++) {

			Element elementAll = elements.get(i);

			allList.add(elementAll.text().trim());

			Elements dls = elementAll.select("dl");

			// OL liste elemanları
			for (int j = 0; j < dls.size(); j++) {
				Element dd = dls.get(j);
				Elements els = dd.select("dd div.h-usage-example");
				for (int k = 0; k < els.size(); k++) {
					String ddTitle = els.get(k).text();
					if (!ddTitle.isEmpty()) {
						// System.err.println(i + ". ddTitle:" + ddTitle);
					}
				}
			}

			Elements uls = elementAll.select("ul li");
			for (int j = 0; j < uls.size(); j++) {
				ulList.add(uls.get(j).text());
			}
		}

		for (int j = 0; j < allList.size(); j++) {
			String allStr = allList.get(j);
			for (int k = 0; k < ulList.size(); k++) {
				String ulStr = ulList.get(k);
				if (allStr.contains(ulStr)) {
					allStr = allStr.replace(ulStr, "");
				}
				allList.set(j, allStr);
			}
		}

		for (int j = 0; j < allList.size(); j++) {
			if (!allList.get(j).isEmpty())
				System.err.println(j + ". allList.get:" + allList.get(j));
		}

	}

	static List<WktMeanPair> findTagToTag2(String fromTag, String toTag) {

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", "on").post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			List<WktMeanPair> list = new ArrayList<>();

			int count = 0;

			int index = 0;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(fromTag) || cssSelector.contains(toTag)) {

					count++;
				}

				if (count == 1) {

					if (checkTagContain(cssSelector)) {

						// System.err.println("cssSelector:" + cssSelector);

						WktMeanPair mainPair = new WktMeanPair(index++, elementTag(elementChild.cssSelector()),
								elementChild.text().trim());

						list.add(mainPair);

						mainPair.setElement(elementChild);
					}
				}
			}

			return list;

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static List<WktMeanPair> findFirstTagList(String fromTag) {

		try {

			Connection con = Jsoup.connect(url);

			con.timeout(30000);

			Document d = con.data("search", "on").post();

			Elements elements = d.select("div#content div#bodyContent div#mw-content-text div.mw-parser-output");

			Element element = elements.get(0);

			Elements elementsChildren = element.children();

			List<WktMeanPair> list = new ArrayList<>();

			int count = 0;

			int index = 0;

			for (int i = 0; i < elementsChildren.size(); i++) {

				Element elementChild = elementsChildren.get(i);

				String cssSelector = elementChild.cssSelector();

				if (cssSelector.contains(fromTag)) {

					count++;
				}

				if (count == 1) {

					if (checkTagContain(cssSelector)) {

						// System.err.println("cssSelector:" + cssSelector);

						WktMeanPair mainPair = new WktMeanPair(index++, elementTag(elementChild.cssSelector()),
								elementChild.text().trim());

						mainPair.setElement(elementChild);

						list.add(mainPair);
					}
				}
			}

			return list;

		} catch (IOException e) {

			e.printStackTrace();
		}

		return null;
	}

	static String elementTag(String cssSelector) {

		if (cssSelector.contains(tagOL)) {
			return tagOL;
		} else if (cssSelector.contains(tagH4)) {
			return tagH4;
		} else if (cssSelector.contains(tagP)) {
			return tagP;
		} else {
			return tagH2;
		}
	}

	static boolean checkTagContain(String cssTag) {

		if (cssTag.contains(tagH4) || cssTag.contains(tagP) || cssTag.contains(tagOL)) {

			return true;
		}

		return false;
	}
}
